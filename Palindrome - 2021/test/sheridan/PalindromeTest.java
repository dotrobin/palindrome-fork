package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular() {
		String word = "dad";
		assertTrue("Unable to validate palindrome word", Palindrome.isPalindrome(word));
	}

	@Test
	public void testIsPalindromeNegative() {
		String word = "word";
		assertFalse("Unable to validate palindrome word", Palindrome.isPalindrome(word));
	}

	@Test
	public void testIsPalindromeBoundaryIn() {
		String word = "edit tide";
		assertTrue("Unable to validate palindrome word", Palindrome.isPalindrome(word));
	}

	@Test
	public void testIsPalindromeBoundaryOut() {
		String word = "edit on tide";
		assertFalse("Unable to validate palindrome word", Palindrome.isPalindrome(word));
	}
}